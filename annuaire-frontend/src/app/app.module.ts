import { APP_INITIALIZER, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { UserListComponent } from './pages/user-list/user-list.component';
import { UserDetailsComponent } from './pages/user-details/user-details.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import {MatButtonModule} from '@angular/material/button';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import { SnackbarContentComponent } from './components/snackbar-content/snackbar-content.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserAddComponent } from './pages/user-add/user-add.component';
import { PrintErrorsDirective } from './directives/print-errors.directive';
import { LoginComponent } from './pages/login/login.component';
import { SecurityService } from './services/security.service';
import { SecurityInterceptor } from './interceptors/security.interceptor';
import { IfAuthenticatedDirective } from './directives/if-authenticated.directive';
import { IfAnonymousDirective } from './directives/if-anonymous.directive';

const materialImports = [
    MatButtonModule,
    MatSnackBarModule,
    MatProgressSpinnerModule
];

@NgModule({
  declarations: [
    AppComponent,
    UserListComponent,
    UserDetailsComponent,
    SnackbarContentComponent,
    UserAddComponent,
    PrintErrorsDirective,
    LoginComponent,
    IfAuthenticatedDirective,
    IfAnonymousDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    materialImports
  ],
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: (ss: SecurityService) => () => ss.init(),
      deps: [SecurityService],
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: SecurityInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
