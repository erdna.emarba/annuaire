import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from '../models/user';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  baseUrl = environment.backendUrl + "users";

  constructor(private httpClient: HttpClient) { }

  findAll(): Observable<User[]> {
    return this.httpClient.get<User[]>(this.baseUrl);
  }

  findById(id: number): Observable<User> {
    return this.httpClient.get<User>(this.baseUrl + "/" + id);
  }

  search(q: string): Observable<User[]> {
    return this.httpClient.get<User[]>(this.baseUrl + "/search?q="+q);
  }

  save(user: User): Observable<User> {
    return this.httpClient.post<User>(this.baseUrl, user);
  }

  delete(user: User): Observable<void> {
    return this.httpClient.delete<void>(this.baseUrl + "/" + user.id);
  }

}
