import { Injectable } from '@angular/core';
import { BehaviorSubject, tap } from 'rxjs';
import { User } from '../models/user';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { JwtPayload, jwtDecode } from 'jwt-decode';

interface JwtResponse {
  accessToken: string;
  refreshToken: string;
}

export interface AuthenticatedUser {
  accessToken: string;
  refreshToken: string;
  username: string;
}

interface JwtContent extends JwtPayload {
  username: string;
}

@Injectable({
  providedIn: 'root'
})
export class SecurityService {

  authenticatedUser: BehaviorSubject<AuthenticatedUser | undefined> = new BehaviorSubject<AuthenticatedUser | undefined>(undefined);

  constructor(private httpClient: HttpClient) { }

  login(email: string, password: string) {
    this.httpClient.post<JwtResponse>(environment.backendUrl + "authenticate", {
      grantType: "password",
      username: email,
      password: password
    }).subscribe(response => {
      const content = jwtDecode<JwtContent>(response.accessToken);
      const au = {
        accessToken: response.accessToken,
        refreshToken: response.refreshToken,
        username: content.username
      };
      this.authenticatedUser.next(au);
      localStorage.setItem("authenticatedUser", JSON.stringify(au));
    });
  }

  logout() {
    this.authenticatedUser.next(undefined);
    localStorage.removeItem("authenticatedUser");
  }

  init() {
    const au = localStorage.getItem("authenticatedUser");
    if (au)
      this.authenticatedUser.next(JSON.parse(au));
  }

  refresh() {
    if (this.authenticatedUser.value === undefined)
      throw new Error("not authenticated");
    return this.httpClient.post<JwtResponse>(environment.backendUrl + "authenticate", {
      grantType: "refreshToken",
      refreshToken: this.authenticatedUser.value?.refreshToken
    }).pipe(tap(response => {
      const content = jwtDecode<JwtContent>(response.accessToken);
      const au = {
        accessToken: response.accessToken,
        refreshToken: response.refreshToken,
        username: content.username
      };
      this.authenticatedUser.next(au);
      localStorage.setItem("authenticatedUser", JSON.stringify(au));
    }));
  }

}
