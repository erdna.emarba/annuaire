import { Component } from '@angular/core';
import { User } from '../../models/user';
import { UserService } from '../../services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-add',
  templateUrl: './user-add.component.html',
  styleUrl: './user-add.component.css'
})
export class UserAddComponent {

  userGroup = User.formGroup();

  constructor(
    private userService: UserService,
    private router: Router
  ) {}

  onSubmit() {
    this.userService.save(this.userGroup.getRawValue()).subscribe(() => this.router.navigateByUrl("/users"));
  }
}
