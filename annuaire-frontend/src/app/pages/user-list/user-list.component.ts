import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { User } from '../../models/user';
import { Observable, debounceTime, mergeMap, startWith } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SnackbarContentComponent } from '../../components/snackbar-content/snackbar-content.component';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrl: './user-list.component.css'
})
export class UserListComponent implements OnInit {

  users?: User[];
  users$?: Observable<User[]>;

  searchControl = new FormControl("");

  constructor(
    private userService: UserService,
    private snackBarService: MatSnackBar
    ) {}

  ngOnInit(): void {
      this.users$ = this.searchControl.valueChanges
        .pipe(
          startWith(""),
          debounceTime(500),
          mergeMap(value => this.userService.search(value!)));
  }

  onDelete(user: User): void {
    const snackBarRef = this.snackBarService.openFromComponent(SnackbarContentComponent, {
      data: "deleting user " + user.id + " ..."
    });
    this.userService.delete(user).subscribe({
      next: () => {
          snackBarRef.dismiss();
          const index = this.users!.findIndex(u => u.id === user.id);
          this.users?.splice(index, 1);
          this.snackBarService.open("Delete succeed :)", undefined, {
            duration: 2000
          })
        },
      error: err => {
        this.snackBarService.open("Delete failed :( status: " + err.status + " message: " + err.error.message, undefined, {
          duration: 5000
        })
      }
    })
  }
}
