import { Component } from '@angular/core';
import { SecurityService } from '../../services/security.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrl: './login.component.css'
})
export class LoginComponent {

  email: string = "";
  password: string = "";

  constructor(private securityService: SecurityService) {}

  onSubmit() {
    this.securityService.login(this.email, this.password);
  }

}
