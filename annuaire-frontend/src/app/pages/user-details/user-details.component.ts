import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserService } from '../../services/user.service';
import { User } from '../../models/user';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrl: './user-details.component.css'
})
export class UserDetailsComponent implements OnInit {

  status: "loading" | "loaded" | "error" = "loading";

  user?: User;
  errorMessage?: string;

  constructor(
    private activatedRoute: ActivatedRoute,
    private userService: UserService
  ) {}

  ngOnInit(): void {
      const id = this.activatedRoute.snapshot.params['id'];
      this.userService.findById(id).subscribe({
        next: user => {
          this.user = user;
          this.status = 'loaded';
        },
        error: err => {
          this.errorMessage = err.status + " - " + err.message;
          this.status = 'error';
        }
      });
  }
}
