import { Component, Inject } from '@angular/core';
import { MAT_SNACK_BAR_DATA } from '@angular/material/snack-bar';

@Component({
  selector: 'app-snackbar-content',
  templateUrl: './snackbar-content.component.html',
  styleUrl: './snackbar-content.component.css'
})
export class SnackbarContentComponent {

  message: string = "default message";

  constructor(@Inject(MAT_SNACK_BAR_DATA) data: string) {
    this.message = data;
  }
}
