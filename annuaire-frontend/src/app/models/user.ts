import { FormArray, FormControl, FormGroup, Validators } from "@angular/forms";
import { City } from "./city";

export interface User {
  id: number;
  name: string;
  email: string;
  password: string;
  cities: City[];
}

export namespace User {

  export const empty: User = { id: 0, name: '', email: '', password: '', cities: []};

  export function formGroup(user?: User) {
    return new FormGroup({
      id: new FormControl(user?.id ?? 0, {nonNullable: true}),
      name: new FormControl(user?.name ?? '', {nonNullable: true}),
      email: new FormControl(user?.email ?? '', { validators: [Validators.email], nonNullable: true}),
      password: new FormControl(user?.password ?? '', { validators: [Validators.minLength(8)], nonNullable: true}),
      cities: new FormArray([])
    });
  }
}
