import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, catchError, mergeMap, throwError } from 'rxjs';
import { SecurityService } from '../services/security.service';

@Injectable()
export class SecurityInterceptor implements HttpInterceptor {

  constructor(private securityService: SecurityService) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const au = this.securityService.authenticatedUser.value;
    if (au && !req.url.includes("authenticate")) {
      req = req.clone({
        headers: req.headers.set("Authorization", "Bearer " + au.accessToken)
      })
    }
    return next.handle(req).pipe(catchError(error => {
      if (error instanceof HttpErrorResponse && !req.url.includes("authenticate") && error.status === 401) {
        return this.securityService.refresh().pipe(
          mergeMap(jwtResponse => {
            req = req.clone({
              headers: req.headers.set("Authorization", "Bearer " + jwtResponse.accessToken)
            })
            return next.handle(req);
          }),
          catchError(err => {
            this.securityService.logout();
            return throwError(() => error);
          })
          )
      } else
        return throwError(() => error);
    }));
  }

}

