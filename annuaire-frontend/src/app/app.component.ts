import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticatedUser, SecurityService } from './services/security.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent implements OnInit {

  authenticatedUser?: AuthenticatedUser;

  constructor(
    private router: Router,
    private securityService: SecurityService) { }

  ngOnInit(): void {
    this.securityService.authenticatedUser.subscribe(au => this.authenticatedUser = au);
  }

  onLogout() {
    this.securityService.logout();
  }
}
