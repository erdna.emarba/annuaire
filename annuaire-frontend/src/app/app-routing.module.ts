import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserListComponent } from './pages/user-list/user-list.component';
import { UserDetailsComponent } from './pages/user-details/user-details.component';
import { UserAddComponent } from './pages/user-add/user-add.component';
import { LoginComponent } from './pages/login/login.component';
import { isAuthenticatedGuard } from './guards/is-authenticated.guard';

const routes: Routes = [

  { path: "demo", loadChildren: () => import("./demos/demos.module").then(m => m.DemosModule) },
  { path: "users/add", component: UserAddComponent, canActivate: [isAuthenticatedGuard] },
  { path: "users/:id", component: UserDetailsComponent },
  { path: "users", component: UserListComponent },
  { path: "login", component: LoginComponent },
  { path: "**", redirectTo: "users"}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
