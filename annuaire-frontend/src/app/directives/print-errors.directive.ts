import { Directive, Input, TemplateRef, ViewContainerRef } from '@angular/core';
import { AbstractControl } from '@angular/forms';
import { Subscription } from 'rxjs';

const defaultMessages: { [k: string]: string } = {
  required: "required field",
  minlength: "too short",
  email: "not a valid email"
}


@Directive({
  selector: '[appPrintErrors]'
})
export class PrintErrorsDirective {

  @Input("appPrintErrors") formControl?: AbstractControl;
  private subscription?: Subscription;

  constructor(
    private templateRef: TemplateRef<any>,
    private viewContainer: ViewContainerRef
  ) { }

  ngOnInit(): void {
    this.subscription = this.formControl?.statusChanges.subscribe(() => {
      this.templateRef.elementRef.nativeElement.previousElementSibling.innerText = "ERROR";
      if (this.formControl?.errors) {
        this.viewContainer.clear();
        this.viewContainer.createEmbeddedView(this.templateRef);
        Object.keys(this.formControl?.errors).forEach(k => {
          this.templateRef.elementRef.nativeElement.previousElementSibling.innerText += defaultMessages[k] ?? this.formControl?.errors?.[k].message;
        })
      } else
        this.viewContainer.clear();
    })
  }

  ngOnDestroy(): void {
      if (this.subscription)
        this.subscription.unsubscribe();
  }

}
