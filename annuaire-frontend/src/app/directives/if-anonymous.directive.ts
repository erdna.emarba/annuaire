import { Directive, OnDestroy, OnInit, TemplateRef, ViewContainerRef } from '@angular/core';
import { Subscription } from 'rxjs';
import { SecurityService } from '../services/security.service';

@Directive({
  selector: '[appIfAnonymous]'
})
export class IfAnonymousDirective implements OnInit, OnDestroy {

  subscription?: Subscription;

  constructor(
    private template: TemplateRef<any>,
    private container: ViewContainerRef,
    private securityService: SecurityService
  ) { }

  ngOnInit(): void {
    this.subscription = this.securityService.authenticatedUser.subscribe(au => {
      if (!au)
        this.container.createEmbeddedView(this.template);
      else
        this.container.clear();
    });
  }

  ngOnDestroy(): void {
    if (this.subscription)
      this.subscription.unsubscribe();
  }

}
