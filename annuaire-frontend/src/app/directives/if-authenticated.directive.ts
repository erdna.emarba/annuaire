import { Directive, OnDestroy, OnInit, TemplateRef, ViewContainerRef } from '@angular/core';
import { SecurityService } from '../services/security.service';
import { Subscription } from 'rxjs';

@Directive({
  selector: '[appIfAuthenticated]'
})
export class IfAuthenticatedDirective implements OnInit, OnDestroy {

  subscription?: Subscription;

  constructor(
    private template: TemplateRef<any>,
    private container: ViewContainerRef,
    private securityService: SecurityService
  ) { }

  ngOnInit(): void {
    this.subscription = this.securityService.authenticatedUser.subscribe(au => {
      if (au)
        this.container.createEmbeddedView(this.template);
      else
        this.container.clear();
    });
  }

  ngOnDestroy(): void {
    if (this.subscription)
      this.subscription.unsubscribe();
  }

}
