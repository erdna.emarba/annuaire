import { inject } from '@angular/core';
import { CanActivateFn, Router } from '@angular/router';
import { SecurityService } from '../services/security.service';

export const isAuthenticatedGuard: CanActivateFn = (route, state) => {
  const securityService = inject(SecurityService);
  const router = inject(Router);
  return (securityService.authenticatedUser.value === undefined) ? router.createUrlTree(["login"]) : true;
};
