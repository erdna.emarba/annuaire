import { Component } from '@angular/core';

@Component({
  selector: 'app-template-driven-forms',
  templateUrl: './template-driven-forms.component.html',
  styleUrl: './template-driven-forms.component.css'
})
export class TemplateDrivenFormsComponent {

  person = {
    name: "",
    age: 40
  };

  onSubmit() {
    console.log(this.person);
  }

}
