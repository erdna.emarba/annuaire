import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-page2',
  templateUrl: './page2.component.html',
  styleUrl: './page2.component.css'
})
export class Page2Component implements OnInit, OnDestroy {

  idFromSnapshot: number = 0;
  idFromObservable: number = 0;

  paramsSubscription?: Subscription;

  constructor(
    private activatedRoute: ActivatedRoute
    ) {}

  ngOnInit(): void {
      this.idFromSnapshot = Number(this.activatedRoute.snapshot.params['id']);
      this.paramsSubscription = this.activatedRoute.params
        .subscribe(params => this.idFromObservable = Number(params['id']));
  }

  ngOnDestroy(): void {
      this.paramsSubscription?.unsubscribe();
  }

}
