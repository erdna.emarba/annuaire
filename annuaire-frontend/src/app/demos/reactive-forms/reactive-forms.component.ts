import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-reactive-forms',
  templateUrl: './reactive-forms.component.html',
  styleUrl: './reactive-forms.component.css'
})
export class ReactiveFormsComponent {

  personForm = new FormGroup({
    name: new FormControl("", [Validators.required, Validators.pattern(/^[a-zA-Z]*$/)]),
    age: new FormControl(40, [(ac) => ac.value > 40 ? { tooOld: { message: "trop vieux !"}} : null, Validators.max(40)])
  });

  onSubmit() {
    console.log(this.personForm);
    console.log(this.personForm.value);
  }

  get nameControl(): FormControl<string> {
    return this.personForm.get('name') as FormControl;
  }
}
