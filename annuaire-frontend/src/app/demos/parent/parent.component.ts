import { Component, ElementRef, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { EnfantComponent } from '../enfant/enfant.component';

@Component({
  selector: 'app-parent',
  templateUrl: './parent.component.html',
  styleUrl: './parent.component.css'
})
export class ParentComponent {

  unAttribut = { name: "andre", age: 40 };
  unAutre = { name: "lucie", age: 11 };

  @ViewChildren(EnfantComponent) enfants?: QueryList<EnfantComponent>;
  @ViewChild("title", {static: false}) title?: ElementRef;

  onChangeCase(name: string): void {
    let person;
    if (name.toLowerCase() === "andre")
      person = this.unAttribut;
    else if (name.toLowerCase() === "lucie")
      person = this.unAutre;
    if (person)
      person.name = RegExp(/^[a-z]/).exec(person.name) ? person.name.toUpperCase() : person.name.toLowerCase();
  }

}
