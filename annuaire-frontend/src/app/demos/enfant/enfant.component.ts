import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-enfant',
  templateUrl: './enfant.component.html',
  styleUrl: './enfant.component.css'
})
export class EnfantComponent {

  @Input() person?: { name: string, age: number };
  @Output() switchCase = new EventEmitter<string>();

  onClick() {
    this.switchCase.emit(this.person!.name);
  }
}
