package fr.formation.annuaire.models;

import java.util.List;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.Table;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Entity
@Table(name = "Utilisateur")
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@SuperBuilder
public class User {

    @Id
    @GeneratedValue
    @EqualsAndHashCode.Include
    private long id;

    private String name;

    private String email;

    private String password;

    @ManyToMany(cascade = CascadeType.PERSIST)
    private List<City> cities;
}
