package fr.formation.annuaire.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.formation.annuaire.models.City;

@Repository
public interface CityRepository extends JpaRepository<City, Long> {
    
}
